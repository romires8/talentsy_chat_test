from django.db import models
from django.contrib.auth.models import User


class TimeMixin(models.Model):
    created_at = models.DateTimeField('Дата создания', auto_now_add=True)
    updated_at = models.DateTimeField('Дата обновления', auto_now=True)

    class Meta:
        abstract = True


class Room(TimeMixin):
    """
    Комната чата
    """
    title = models.CharField(max_length=255)
    sort = models.IntegerField(default=0)

    def __str__(self):
        return self.title

    @property
    def group_name(self):
        return "room-%s" % self.id

    class Meta:
        verbose_name = 'Комната чата'
        verbose_name_plural = 'Комнаты чата'


class Message(TimeMixin):
    """
    Сообщение в чате
    """
    room = models.ForeignKey(Room, on_delete=models.PROTECT)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    content = models.TextField()
    has_read = models.BooleanField(default=False)

    def __str__(self):
        return self.content

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'
