from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from .models import Room


@login_required
def index(request, room_id=None):
    rooms = Room.objects.order_by("sort", "title")

    return render(request, "index.html", {
        "rooms": rooms,
        "room_id": room_id
    })
